CREATE USER 'repl'@'%' IDENTIFIED BY 'welcome1'; 
GRANT all ON spaceballstock.* TO 'repl'@'%'; 
GRANT alter,create,drop,index,insert,update,delete,select on spaceballstock.* TO 'repl'@'%'; 
GRANT REPLICATION CLIENT ON *.* TO 'repl'@'%';
GRANT file on *.* to 'repl'@'%';

USE spaceballstock; 

SHOW GLOBAL VARIABLES LIKE 'local_infile';
SET GLOBAL local_infile=1;

create table Encrypt_Trader(id int, encrypted_frist_name varchar(100), encrypted_last_name varchar(100), encrypted_user_name varchar(100), encrypted_password varchar(100), encrypted_email varchar(100), primary key(id));
LOAD DATA LOCAL INFILE '/Users/sophiehou/Desktop/622/projreport/data/userfile_Encrpyted.csv' 
INTO TABLE Encrypt_Trader 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES
(@id, @encrypted_frist_name, @encrypted_last_name, @encrypted_user_name, @encrypted_password, @encrypted_email) 
SET id=IF(@id='',NULL,@id), encrypted_frist_name=IF(@encrypted_frist_name='',NULL,@encrypted_frist_name), encrypted_last_name=IF(@encrypted_last_name='',NULL,@encrypted_last_name), encrypted_user_name=IF(@encrypted_user_name='',NULL,@encrypted_user_name),encrypted_password=IF(@encrypted_password='',NULL,@encrypted_password), encrypted_email=IF(@encrypted_email='',NULL,@encrypted_email);
SELECT * FROM Encrypt_Trader;
 

create table Encrypt_Trade (user_id INT, encrypted_username varchar(100), encrypted_stock_ticker varchar(100), encrypted_buy_sell varchar(100), encrypted_date varchar(100), encrypted_quantity varchar(100), price DECIMAL(10,2), encrypted_total_price varchar(100)
primary key(user_id, encrypted_stock_ticker, encrypted_date), FOREIGN KEY(user_id) REFERENCES Trader(id));
LOAD DATA LOCAL INFILE '/Users/sophiehou/Desktop/622/projreport/data/dataset/encrypted\ dataset/Buy_Sell_Encrpyted.csv' 
INTO TABLE Encrypt_Trade
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES
(@user_id, @encrypted_username, @encrypted_stock_ticker, @encrypted_buy_sell, @encrypted_date, @encrypted_quantity, @price, @encrypted_total_price)
SET user_id=IF(@user_id='',NULL,@user_id), encrypted_username=IF(@encrypted_username='',NULL,@encrypted_username), encrypted_stock_ticker=IF(@encrypted_stock_ticker='',NULL,@encrypted_stock_ticker), encrypted_buy_sell=IF(@encrypted_buy_sell='',NULL,@encrypted_buy_sell), encrypted_date=IF(@encrypted_date='',NULL,@encrypted_date), encrypted_quantity=IF(@encrypted_quantity='',NULL,@encrypted_quantity), price=IF(@price='',NULL,@price), encrypted_total_price=IF(@encrypted_total_price='',NULL,@encrypted_total_price);

SELECT * FROM Encrypt_Trade;



create table Stocks (ticker varchar(100), price DECIMAL(10,2),  date varchar(100), PE_Ratio DECIMAL(10,2), Dividend_Rate DECIMAL(10,2),
primary key(ticker));

LOAD DATA LOCAL INFILE '/Users/sophiehou/Downloads/data/stockfile.csv' 
INTO TABLE Stocks
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES
(@ticker, @price, @date, @PE_Ratio, @Dividend_Rate)
SET ticker=IF(@ticker='',NULL,@ticker), price=IF(@price='',NULL,@price), date=IF(@date='',NULL,@date), PE_Ratio=IF(@PE_Ratio='',NULL,@PE_Ratio), Dividend_Rate=IF(@Dividend_Rate='',NULL,@Dividend_Rate);

mysql> SELECT * FROM Stocks;



create table porfoilo (ticker varchar(100), user_id INT, quantity INT,
primary key(user_id));

LOAD DATA LOCAL INFILE '/Users/sophiehou/Downloads/data/porfoilo.csv' 
INTO TABLE porfoilo
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES
(@ticker, @user_id, @quantity)
SET ticker=IF(@ticker='',NULL,@ticker), user_id=IF(@user_id='',NULL,@user_id), quantity=IF(@quantity='',NULL,@quantity);

mysql> SELECT * FROM porfoilo;



create table trader_id_porfoilo (trader_id INT, porfoilo DECIMAL(10,2),
primary key(trader_id));

LOAD DATA LOCAL INFILE '/Users/sophiehou/Downloads/data/porfoilo_trader_id_porfolio.csv' 
INTO TABLE trader_id_porfoilo
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES
(@trader_id, @porfoilo)
SET trader_id=IF(@trader_id='',NULL,@trader_id), porfoilo=IF(@porfoilo='',NULL,@porfoilo);

mysql> SELECT * FROM trader_id_porfoilo;

